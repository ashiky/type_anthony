//parametrage des inputs
const readline = require("readline");
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

// fonction pour convertir les différentes devises via des inputs dans le terminal
const convertCurrency = (): void => {
    rl.question("Quelle est le montant et votre devise ? exemple '10 EUR ' \n", function (baseCurrency: string) {
        rl.question("En quelle devise voulez vous convertir ? 'EUR', 'CAD' ou 'JPY'\n ", function (newCurrency: string) {
            let num: number = Number(baseCurrency.split(" ")[0])
            const currency: string = baseCurrency.split(" ")[1]
            if (currency == "EUR") {
                switch (newCurrency) {
                    case "CAD":
                        num *= 1.5
                        break
                    case "JPY":
                        num *= 1.3
                        break
                    default:
                        console.log("Une erreur est survenu, merci de réessayer")
                }
            } else if (currency == "CAD") {
                switch (newCurrency) {
                    case "EUR":
                        num *= 0.67
                        break
                    case "JPY":
                        num *= 87
                        break
                    default:
                        console.log("Une erreur est survenu, merci de réessayer")
                }
            } else if (currency == "JPY") {
                switch (newCurrency) {
                    case "EUR":
                        num *= 0.0077
                        break
                    case "CAD":
                        num *= 0.0115
                        break
                    default:
                        console.log("Une erreur est survenu, merci de réessayer")
                }

            }
            console.log(`${num} ${newCurrency}`)
            launch()
        });
    });
}

// calcul des frais de livraison
const calculateDeliveryCost = (): void => {
    let extracost: number
    let cost: number
    let currency: string
    let totalCM: number
    rl.question("Quelle est la masse de votre coli ?\n", (mass: number) => {
        rl.question("Quelle est la heuteur de votre coli ?\n", (h: number) => {
            rl.question("Quelle est la largeur de votre coli ?\n", (w: number) => {
                rl.question("Quelle est la longueur de votre coli ?\n", (l: number) => {
                    rl.question("Quelle est le pays de destination ?\n", (country: string) => {
                        totalCM = Number(l) + Number(h) + Number(w)
                        switch (country) {
                            case "france":
                                totalCM > 150 ? extracost = 5 : 0
                                currency = "EUR"
                                if (Number(Number(mass)) <= 1) {
                                    console.log('ma france')
                                    console.log(cost)
                                    cost = 10
                                    console.log(cost)
                                    console.log('ma france')
                                    break
                                } else if (Number(mass) <= 3) {
                                    cost = 20
                                    break
                                } else {
                                    cost = 30
                                    break
                                }
                            case "canada":
                                totalCM > 150 ? extracost = 7.5 : 0
                                currency = "CAD"
                                if (Number(mass) <= 1) {
                                    cost = 15
                                    break
                                } else if (Number(mass) <= 3) {
                                    cost = 30
                                    break
                                } else {
                                    cost = 45
                                    break
                                }
                            case "japon":
                                totalCM > 150 ? extracost = 500 : 0
                                currency = "JPY"
                                if (Number(mass) <= 1) {
                                    cost = 1000
                                    break
                                } else if (Number(mass) <= 3) {
                                    cost = 2000
                                    break
                                } else {
                                    cost = 3000
                                    break
                                }
                            default:
                                console.log("Une erreur est survenu, merci de réessayer")
                        }
                        console.log(totalCM)
                        console.log(extracost)
                        console.log(cost)
                        extracost != 0
                            ? console.log(`Votre livraison couteras un total de ${cost + extracost} dont ${extracost} de frais suplémentaire`)
                            : console.log(`Votre livraison couteras un total de ${cost}`)
                            launch()
                    })
                })
            })
        })
    })
}

const customsCost = (): void => {
    rl.question("Quelle est le pays de destination ?\n", (country: string): void => {
        rl.question("Quelle la valeur de votre colis ?\n", (value: string): void => {
            let num: number = Number(value)
            let currency: string = ""
            switch (country) {
                case "canada":
                    currency = "CAD"
                    if (num > 20) {
                        num += (15 / 100) * num
                        break
                    }
                    break
                case "japon":
                    currency = "JPY"
                    if (num > 20) {
                        num += (10 / 100) * num
                        break
                    }
                    break
                default:
                    console.log("Une erreur est survenu, merci de réessayer")
            }
            console.log(`Cela vous coutera ${num} ${currency}`)
            launch()
        })
    })
}

//fonction de lancement du logicielle
const launch = (): void => {
    rl.question("'convertir'\nPour convertir un montant dans une autre devise.\n'livraison'\nPour connaitre les frais de livraison d'un colis\n'douanes'\n Pour connaitre les frais de douane\n", (input: string) => {
        switch (input) {
            case "convertir":
                convertCurrency()
                break
            case "livraison":
                calculateDeliveryCost()
                break
            case "douanes":
                customsCost()
            default:
                console.log('erreur, réessayer')
                launch()
        }
    })
}

    launch()
